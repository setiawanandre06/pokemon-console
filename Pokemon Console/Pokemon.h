#pragma once
#include "base.h"
#include<iostream>
#include<string>

enum class EBasicType
{
	None,
	Normal, 
	Fire, 
	Fighting, 
	Water, 
	Flying, 
	Grass, 
	Poison, 
	Electric, 
	Ground, 
	Psychic,
	Rock,
	Ice,
	Bug,
	Dragon,
	Ghost,
	Dark,
	Steel,
	Fairy
};

enum class EGender {
	Male,
	Female,
	Both,
	Unknown
};

enum class ECategory
{
	Physical,
	Special
};

struct FType 
{
	EBasicType Main, Sub;

	FType();
	FType(EBasicType);
	FType(EBasicType, EBasicType);
	void AddSubTypes(EBasicType);
};

class FMoves
{
	FString MoveName, Description;
	EBasicType MoveType;
	ECategory MoveCategory;
	int32 Power, Accuracy;

public:
	FMoves();
	FMoves(FString, EBasicType, ECategory, int32, int32, FString);
};

class FStats
{
	int32 HP, Attack, Defense, SpAtk, SpDef, Speed, Total;

	// constructor, set & get
public:
	FStats();
	void SetStats(int32, int32, int32, int32, int32, int32);
	int32 GetTotal();
};

struct FPokemon
{
	FString Name, Species;
	int32 PokedexID, NationalDexID;
	float Height, Weight; // height in m, weight in kg
	FType Type;
	EGender Gender;

	FPokemon(int, int, FString, FType, EGender, FString, float, float);
};

