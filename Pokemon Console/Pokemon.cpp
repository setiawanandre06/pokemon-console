#include "Pokemon.h"

FType::FType()
{

}

FType::FType(EBasicType main)
{
	Main = main; 
}

FType::FType(EBasicType main, EBasicType sub)
{
	Main = main;
	Sub = sub;
}

void FType::AddSubTypes(EBasicType sub)
{
	Sub = sub;
}

FType operator|(EBasicType main, EBasicType sub)
{
	return FType::FType(main, sub);
}

FMoves::FMoves()
{
	MoveName = "";
}

FMoves::FMoves(FString move_name, EBasicType move_type, ECategory move_category, int32 power, int32 accuracy, FString description)
{
	MoveName = move_name;
	MoveType = move_type;
	MoveCategory = move_category;
	Power = power;
	Accuracy = accuracy;
	Description = description;
}

// MissingNo stats will be constructors / base stats
FStats::FStats()
{
	HP = 33;
	Attack = 136;
	Defense = 0;
	SpAtk = 6;
	SpDef = 6;
	Speed = 29;
	Total = FStats::GetTotal();
}

// Set Pokemon Stats
void FStats::SetStats(int32 hp, int32 attack, int32 defense, int32 sp_atk, int32 sp_def, int32 speed)
{
	HP = hp;
	Attack = attack;
	Defense = defense;
	SpAtk = sp_atk;
	SpDef = sp_def;
	Speed = speed;
	Total = FStats::GetTotal();
}

// Get Total Stats
int32 FStats::GetTotal()
{
	return HP + Attack + Defense + SpAtk + SpDef + Speed;
}

FPokemon::FPokemon(int pokedex_id, int national_pokedex_id, FString name, FType type, EGender gender, FString species, float height, float width)
{
	Name = name;
	Species = species;
	PokedexID = pokedex_id;
	NationalDexID = national_pokedex_id;
	Type = type;
	Gender = gender;
	Height = height;
	Weight = width;
}